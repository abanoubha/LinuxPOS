# LinuxPOS
Point-of-Sale Management Software for Elementary OS

_NOTE:_ this software is under heavy development.

## Tech Stack
- [Vala](https://valadoc.org/)
- [GTK](https://valadoc.org/gtk+-3.0/Gtk.html)
- [Granite](https://github.com/elementary/granite)

## ToDo

- [ ] UI / UX design mockup
- [ ] UI / UX design code and/or \*.ui file
- [ ] code skeleton
- [ ] i18n translations
- [ ] business logic

## Alternatives

- [POS in Flutter](https://github.com/abanoubhannaazer/posflutter) (cross platform)
- [POS in Go](https://github.com/abanoubhannaazer/pos) (cross platform)
- [POS in Vala](https://github.com/abanoubhannaazer/LinuxPOS) (this) (Linux : Vala + GTK+ 3)
